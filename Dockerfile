FROM registry.karbowiak.dev/php/php:php8.0

RUN echo "apc.enabled=1" >> /etc/php/8.0/cli/php.ini && \
echo "apc.enable_cli=1" >> /etc/php/8.0/cli/php.ini && \
echo "apc.ttl=7200" >> /etc/php/8.0/cli/php.ini && \
echo "apc.shm_size=32M" >> /etc/php/8.0/cli/php.ini

# Switch working directory
WORKDIR /var/www

# Copy codebase
COPY . ./

# Composer
RUN composer install -o && rm -rf /root/.composer

# Cleanup the container image
RUN apt clean && apt autoclean -y && apt autoremove -y && rm -rf /var/lib/{apt,dpkg,cache,log}/

# Start bot
ENTRYPOINT ["/usr/bin/php", "/var/www/bin/sovereign", "run:bot"]
