[![Build Status](https://drone.karbowiak.dk/api/badges/karbowiak/Sovereign/status.svg)](https://drone.karbowiak.dk/karbowiak/Sovereign)
# Sovereign

> _[I am the vanguard of your destruction, this exchange is just beginning...](https://www.youtube.com/watch?v=R_NAoNd4YyY)_

# Requirements
- PHP 8.0

# Installation
1. composer install

# How to run
- php bin/sovereign run:bot

# Twitch
1. Make a developer account
2. Make clientID
3. Go to https://twitchapps.com/tokengen/ and input client ID
4. Get oauth token
5. steps probably not right, but you get the idea!
6. success