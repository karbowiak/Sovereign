<?php

namespace Sovereign\Api;

use Sovereign\Plugins\Plugins;

class CommonInterface
{
    protected string $trigger = '';
    protected array $extraTriggers = [];
    protected string $description = '';
    protected Plugins $plugins;

    public function getTrigger(): string
    {
        return $this->trigger;
    }

    public function getTriggers(): array
    {
        return array_merge([$this->trigger], $this->extraTriggers);
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setPlugins(Plugins $plugins): void
    {
        $this->plugins = $plugins;
    }
}
