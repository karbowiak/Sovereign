<?php

namespace Sovereign\Api;

use MongoDB\DeleteResult;
use MongoDB\UpdateResult;
use MongoDB\InsertOneResult;
use MongoDB\BSON\UTCDateTime;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ArrayShape;

interface MongoCollectionInterface
{
    public function find(
        array $filter = [],
        #[ArrayShape(['limit' => 'int', 'projection' => 'array', 'sort' => 'array', 'allowDiskUse' => 'bool', 'maxTimeMS' => 'int', 'hint' => 'string'])]
        array $options = [],
        bool $showHidden = false
    ): Collection;
    public function findOne(
        array $filter = [],
        #[ArrayShape(['limit' => 'int', 'projection' => 'array', 'sort' => 'array', 'allowDiskUse' => 'bool', 'maxTimeMS' => 'int', 'hint' => 'string'])]
        array $options = [],
        bool $showHidden = false
    ): Collection;
    public function aggregate(
        #[ArrayShape(['$match' => 'array', '$unwind' => 'array', '$group' => 'array', '$project' => 'array', '$sort' => 'array', '$limit' => 'array'])]
        array $pipeline = [],
        #[ArrayShape(['allowDiskUse' => 'bool', 'maxTimeMS' => 'int', 'hint' => 'string'])]
        array $options = []
    );
    public function delete(array $filter = []): DeleteResult;
    public function update(array $filter = [], array $update = []): UpdateResult;
    public function truncate(): void;
    public function setData(array $data = [], bool $clear = false): void;
    public function getData(): Collection;
    public function saveMany(): void;
    public function save(): UpdateResult|InsertOneResult;

    public function clear(array $data = []): self;
    public function count(
        array $filter = [],
        #[ArrayShape(['limit' => 'int', 'projection' => 'array', 'sort' => 'array', 'allowDiskUse' => 'bool', 'maxTimeMS' => 'int', 'hint' => 'string'])]
        array $options = [],
    ): int;
    public function makeTimeFromDateTime(string $dateTime): UTCDateTime;
    public function makeTimeFromUnixTime(int $unixTime): UTCDateTime;
    public function makeTime(string|int $time): UTCDateTime;
}
