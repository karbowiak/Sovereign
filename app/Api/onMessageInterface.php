<?php

namespace Sovereign\Api;

use Discord\Discord;
use Discord\Parts\Channel\Message;

abstract class onMessageInterface
{
    // @TODO make this return a promise
    abstract public function handle(Discord $discord, Message $message): void;
}
