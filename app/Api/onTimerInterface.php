<?php

namespace Sovereign\Api;

use Discord\Discord;

abstract class onTimerInterface
{
    protected int $interval = 300;

    // @TODO make this return a promise
    abstract public function handle(Discord $discord): void;

    public function getInterval(): int
    {
        return $this->interval;
    }
}
