<?php

namespace Sovereign\Api;

use Discord\Discord;
use Discord\Parts\Channel\Message;

abstract class onTriggerInterface extends CommonInterface
{
    // @TODO make this return a promise
    abstract public function handle(Discord $discord, Message $message, array $parts): void;
}
