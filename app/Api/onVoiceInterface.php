<?php

namespace Sovereign\Api;

use Discord\Discord;
use Discord\Parts\Channel\Channel;
use Discord\Parts\Channel\Message;
use Discord\Parts\Guild\Guild;
use Discord\Repository\Guild\ChannelRepository;
use Discord\Voice\VoiceClient;
use Discord\WebSockets\Events\VoiceStateUpdate;
use Exception;
use React\Promise\ExtendedPromiseInterface;

abstract class onVoiceInterface extends CommonInterface
{
    abstract public function handle(VoiceClient $vc, Discord $discord): ExtendedPromiseInterface;

    public function joinVoice(Discord $discord, Message $message): void
    {
        $channels = $this->getChannelsForGuild($discord, $message->guild_id);

        /** @var Channel $channel */
        foreach ($channels as $channel) {
            if (
                $channel->bitrate !== null &&
                $channel->members->count() > 0 &&
                $this->isUserInChannel($message->author->id, $channel)
            ) {
                $discord->joinVoiceChannel($channel)->then(function (VoiceClient $vc) use ($discord) {
                    $this->handle($vc, $discord)->then(function () use ($vc) {
                        $vc->close();
                    });
                });
            }
        }
    }

    private function isUserInChannel(string $userId, Channel $channel): bool
    {
        /** @var VoiceStateUpdate $member */
        foreach ($channel->members as $member) {
            if ($member->user->id === $userId) {
                return true;
            }
        }

        return false;
    }

    private function getChannelsForGuild(Discord $discord, string $guildId): ChannelRepository
    {
        $guilds = $discord->guilds;
        /** @var Guild $guild */
        foreach ($guilds as $guild) {
            if ($guild->id === $guildId) {
                return $guild->channels;
            }
        }
    }
}
