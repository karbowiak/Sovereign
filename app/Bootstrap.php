<?php

namespace Sovereign;

use Composer\Autoload\ClassLoader;
use League\Container\Container;
use League\Container\ReflectionContainer;
use Psr\Container\ContainerInterface;
use Sovereign\Helpers\Config;
use Sovereign\Plugins\Plugins;

class Bootstrap
{
    public function __construct(
        protected ClassLoader $autoloader,
        public ?ContainerInterface $container = null
    ) {
        $this->buildContainer();
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    protected function buildContainer(): void
    {
        $this->container = $this->container ?? new Container();

        // Automatically resolve classes into the constructor
        $this->container->delegate(
            new ReflectionContainer()
        );

        // Add the config
        $this->container->add(Config::class, new Config());

        // Add the autoloader
        $this->container->add(ClassLoader::class, $this->autoloader);

        // Load plugins
        $this->container->add(Plugins::class, new Plugins($this->autoloader, $this->container));

        // Add the container itself to the container
        $this->container->add(Container::class, $this->container);
    }
}
