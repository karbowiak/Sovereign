<?php

namespace Sovereign\Commands;

use Discord\Discord;
use Discord\Exceptions\IntentException;
use Discord\Parts\Channel\Message;
use Discord\WebSockets\Event;
use New3den\Console\ConsoleCommand;
use Sovereign\Api\onMessageInterface;
use Sovereign\Api\onTimerInterface;
use Sovereign\Helpers\Logger;
use Sovereign\Plugins\Plugins;
use Sovereign\Sovereign;

class RunBot extends ConsoleCommand
{
    protected string $signature = 'run:bot';
    protected string $description = 'Run the Discord bot';

    public function __construct(
        protected Plugins $plugins,
        protected Logger $logger,
        protected Sovereign $sovereign
    ) {
        parent::__construct();
    }

    /**
     * @throws IntentException
     */
    public function handle(): void
    {
        $bot = $this->sovereign->getBotInstance();

        $bot->on('ready', function (Discord $discord) {
            // Set the start time
            $_ENV['startTime'] = time();

            // Get the bot loop
            $loop = $this->sovereign->getLoop();

            // Load all Timers
            /** @var onTimerInterface $timer */
            foreach ($this->plugins->getOnTimer() as $timer) {
                $loop->addPeriodicTimer($timer->getInterval(), function () use ($timer, $discord) {
                    $timer->handle($discord);
                });
            }

            $discord->on(Event::MESSAGE_CREATE, function (Message $message, Discord $discord) {
                /** @var onMessageInterface $messagePlugin */
                foreach ($this->plugins->getOnMessage() as $messagePlugin) {
                    $messagePlugin->handle($discord, $message);
                }

                // Handle plugins
                $prefix = '!';
                if (str_starts_with($message->content, $prefix)) {
                    $parts = explode(' ', $message->content);
                    $trigger = str_replace($prefix, '', $parts[0]);
                    unset($parts[0]);

                    $onTriggerPlugin = $this->plugins->getOnTrigger($trigger);
                    if ($onTriggerPlugin !== null) {
                        $onTriggerPlugin->setPlugins($this->plugins);
                        $onTriggerPlugin->handle($discord, $message, $parts);
                    }

                    $onVoicePlugin = $this->plugins->getOnVoice($trigger);
                    if ($onVoicePlugin !== null) {
                        $onVoicePlugin->setPlugins($this->plugins);
                        $onVoicePlugin->joinVoice($discord, $message);
                    }
                }
            });
        });

        $bot->on('error', function (\Exception $error, $websocket) {
            $this->logger->info('An error occurred', ['error' => $error->getMessage()]);
            $this->sovereign->exit();
        });

        $bot->on('close', function ($opCode, $reason) {
            $this->logger->info(
                'Connection was closed',
                ['code' => $opCode, 'reason' => $reason]
            );

            $this->sovereign->exit();
        });

        $bot->on('reconnecting', function () {
            $this->logger->info('Reconnecting to server');
        });

        $bot->on('reconnected', function () {
            $this->logger->info('Reconnect successful');
        });

        $bot->run();
    }
}
