<?php

namespace Sovereign\Commands;

use League\Container\Container;
use New3den\Console\ConsoleCommand;
use Sovereign\Database\MongoConnection;
use Sovereign\Models\Eightball;
use Sovereign\Models\Meme;
use Sovereign\Models\Nudes;
use Sovereign\Models\Twitch;
use function Safe\file_get_contents;
use function Safe\json_decode;

class Seed extends ConsoleCommand
{
    protected string $signature = 'seed';
    protected string $description = 'Seed the database with data';

    public function __construct(
        protected Container $container
    ) {
        parent::__construct();
    }
    /**
     * @throws IntentException
     */
    public function handle(): void
    {
        $indexes = glob(dirname(__DIR__, 2) . '/resources/indexes/*.yaml');
        /** @var \Sovereign\Database\MongoConnection $connection */
        $connection = $this->container->get(MongoConnection::class);
        foreach ($indexes as $index) {
            $data = \yaml_parse_file($index);
            $collection = $data['collection'];
            foreach ($data['index'] as $index) {
                $connection->connect()->selectCollection('sovereign', $collection)->createIndex($index['key'], ['unique' => $index['unique'] ?? false]);
            }
        }

        // Eightball
        $data = json_decode(file_get_contents(dirname(__DIR__, 2) . '/resources/seeds/eightball.json'));
        $eightballModel = $this->container->get(Eightball::class);
        foreach ($data as $row) {
            $eightballModel->collection->insertOne(['result' => $row]);
        }

        // Meme
        $data = json_decode(file_get_contents(dirname(__DIR__, 2) . '/resources/seeds/meme.json'));
        $memeModel = $this->container->get(Meme::class);
        foreach ($data as $row) {
            $memeModel->collection->insertOne(['result' => $row]);
        }

        // Nudes (Reddit?)
        $data = json_decode(file_get_contents(dirname(__DIR__, 2) . '/resources/seeds/nudes.json'));
        $nudesModel = $this->container->get(Nudes::class);
        foreach ($data as $row) {
            $nudesModel->collection->insertOne(['result' => $row]);
        }

        // Twitch
        $data = json_decode(file_get_contents(dirname(__DIR__, 2) . '/resources/seeds/twitch.json'));
        $twitchModel = $this->container->get(Twitch::class);
        foreach ($data as $row) {
            $twitchModel->collection->insertOne(['name' => $row]);
        }
    }
}
