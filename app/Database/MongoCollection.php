<?php

namespace Sovereign\Database;

use Exception;
use MongoDB\Client;
use RuntimeException;
use MongoDB\UpdateResult;
use MongoDB\DeleteResult;
use MongoDB\GridFS\Bucket;
use MongoDB\InsertOneResult;
use MongoDB\BSON\UTCDateTime;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ArrayShape;
use Sovereign\Api\MongoCollectionInterface;
use Traversable;

class MongoCollection implements MongoCollectionInterface
{
    /** @var string Name of collection in database */
    public string $collectionName = '';
    /** @var string Name of database that the collection is stored in */
    public string $databaseName = 'sovereign';
    /** @var \MongoDB\Collection MongoDB Collection */
    public \MongoDB\Collection $collection;
    /** @var Bucket MongoDB GridFS Bucket for storing files */
    public Bucket $bucket;
    /** @var string Primary index key */
    public string $indexField = '';
    /** @var string Seed file name */
    public string $seedFile = '';
    /** @var string[] $hiddenFields Fields to hide from output (ie. Password hash, email etc.) */
    public array $hiddenFields = [];
    /** @var string[] $required Fields required to insert data to model (ie. email, password hash, etc.) */
    public array $required = [];
    /** @var Collection Data collection when storing data */
    protected Collection $data;
    /** @var Client MongoDB client connection */
    private Client $client;

    public function __construct(
        MongoConnection $mongoConnection
    ) {
        $this->client = $mongoConnection->connect();
        $this->collection = $this->client
            ->selectDatabase($this->databaseName)
            ->selectCollection(!empty($this->collectionName) ? $this->collectionName : 'default');
        $this->bucket = $this->client
            ->selectDatabase($this->databaseName)
            ->selectGridFSBucket();

        $this->data = new Collection();
    }

    public function find(
        array $filter = [],
        #[ArrayShape([
            'limit' => 'int',
            'projection' => 'array',
            'sort' => 'array',
            'allowDiskUse' => 'bool',
            'maxTimeMS' => 'int',
            'hint' => 'string'
        ])] array $options = [],
        bool $showHidden = false
    ): Collection {
        try {
            $result = $this->collection->find($filter, $options)->toArray();

            if ($showHidden) {
                return collect($result);
            }

            return (collect($result))->forget($this->hiddenFields);
        } catch (Exception $e) {
            throw new RuntimeException('Error running query: ' . $e->getMessage());
        }
    }

    public function findOne(
        array $filter = [],
        #[ArrayShape([
            'limit' => 'int',
            'projection' => 'array',
            'sort' => 'array',
            'allowDiskUse' => 'bool',
            'maxTimeMS' => 'int',
            'hint' => 'string'
        ])] array $options = [],
        bool $showHidden = false
    ): Collection {
        try {
            $result = $this->find($filter, $options, $showHidden)->first();

            return collect($result);
        } catch (Exception $e) {
            throw new RuntimeException('Error running query: ' . $e->getMessage());
        }
    }

    public function aggregate(
        #[ArrayShape([
            '$match' => 'array',
            '$unwind' => 'array',
            '$group' => 'array',
            '$project' => 'array',
            '$sort' => 'array',
            '$limit' => 'array'
        ])]
        array $pipeline = [],
        #[ArrayShape([
            'allowDiskUse' => 'bool',
            'maxTimeMS' => 'int',
            'hint' => 'string'
        ])]
        array $options = []
    ): Traversable {
        return $this->collection->aggregate($pipeline, $options);
    }

    public function delete(array $filter = []): DeleteResult
    {
        if (empty($filter)) {
            throw new RuntimeException('Error, filter cannot be empty when deleting');
        }

        return $this->collection->deleteOne($filter);
    }

    public function update(array $filter = [], array $update = [], array $options = []): UpdateResult
    {
        if (empty($filter)) {
            throw new RuntimeException('Error, filter cannot be empty when updating');
        }
        return $this->collection->updateOne($filter, $update, $options);
    }

    final public function truncate(): void
    {
        try {
            $this->collection->drop();
        } catch (Exception $e) {
            throw new RuntimeException('Error truncating collection: ' . $e->getMessage());
        }
    }

    final public function setData(array $data = [], bool $clear = false): void
    {
        if ($clear === true) {
            $this->data = collect();
        }

        foreach ($data as $key => $value) {
            $this->data->merge([$key => $value]);
        }
    }

    final public function getData(): Collection
    {
        return $this->data;
    }

    public function saveMany(): void
    {
        $this->collection->insertMany($this->data->all());
    }

    public function save(): UpdateResult|InsertOneResult
    {
        $this->hasRequired();
        try {
            $session = $this->client->startSession();
            $session->startTransaction([]);
            try {
                $result = $this->collection->insertOne($this->data->toArray(), ['session' => $session]);
                $session->commitTransaction();

                return $result;
            } catch (Exception $e) {
                $session->abortTransaction();
                throw new RuntimeException('Error occurred during transaction: ' . $e->getMessage());
            }
        } catch (Exception $e) {
            try {
                $session = $this->client->startSession();
                $session->startTransaction([]);

                try {
                    $result = $this->collection->replaceOne(
                        [
                            $this->indexField => $this->data->get($this->indexField)
                        ],
                        $this->data->toArray(),
                        [
                            'upsert' => true,
                            'session' => $session
                        ]
                    );
                    $session->commitTransaction();

                    return $result;
                } catch (Exception $e) {
                    $session->abortTransaction();
                    throw new RuntimeException('Error occurred during transaction: ' . $e->getMessage());
                }
            } catch (Exception $ex) {
                throw new RuntimeException("Error saving data: {$ex->getMessage()} / {$e->getMessage()}");
            }
        }
    }

    final public function clear(array $data = []): MongoCollectionInterface
    {
        $this->data = collect(!empty($data) ? $data : null);
        return $this;
    }

    final public function count(
        array $filter = [],
        #[ArrayShape([
            'limit' => 'int',
            'projection' => 'array',
            'sort' => 'array',
            'allowDiskUse' => 'bool',
            'maxTimeMS' => 'int',
            'hint' => 'string'
        ])]
        array $options = []
    ): int {
        return $this->collection->countDocuments($filter, $options);
    }

    final public function makeTimeFromDateTime(string $dateTime): UTCDateTime
    {
        $unixTime = strtotime($dateTime);
        $milliseconds = $unixTime * 1000;

        return new UTCDateTime($milliseconds);
    }

    final public function makeTimeFromUnixTime(int $unixTime): UTCDateTime
    {
        $milliseconds = $unixTime * 1000;

        return new UTCDateTime($milliseconds);
    }

    final public function makeTime(string|int $time): UTCDateTime
    {
        if (is_int($time)) {
            return $this->makeTimeFromUnixTime($time);
        }
        return $this->makeTimeFromDateTime($time);
    }

    final public function hasRequired(Collection $data = null): bool
    {
        if (!empty($this->required)) {
            foreach ($this->required as $key) {
                if ($data !== null && !$data->has($key)) {
                    throw new RuntimeException('Error: ' . $key . ' does not exist in data..');
                }
                if (!$this->data->has($key)) {
                    throw new RuntimeException('Error: ' . $key . ' does not exist in data..');
                }
            }
        }

        return true;
    }
}
