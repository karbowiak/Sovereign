<?php

namespace Sovereign\Database;

use MongoDB\Client;
use Sovereign\Helpers\Config;

class MongoConnection
{
    public function __construct(
        protected Config $config
    ) {
    }

    public function getConnectionString(): string
    {
        $dbServers = explode(',', $this->config->mongoHost);
        $dbOptions = explode(',', $this->config->mongoOptions);
        $connectString = 'mongodb://';
        foreach ($dbServers as $server) {
            $connectString .= $server . ',';
        }
        $connectString = rtrim($connectString, ',');
        $connectString .= '/?' . implode('&', $dbOptions);
        return $connectString;
    }

    public function connect(): Client
    {
        $dbOptions = explode(',', $this->config->mongoOptions);
        return new Client(
            $this->getConnectionString(),
            $dbOptions,
            [
                'typeMap' => [
                    'root' => 'array',
                    'document' => 'array',
                    'array' => 'array'
                ]
            ]
        );
    }
}
