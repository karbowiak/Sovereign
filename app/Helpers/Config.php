<?php

namespace Sovereign\Helpers;

use Illuminate\Support\Collection;

class Config
{
    protected Collection $config;

    public function __construct()
    {
        $configPath = dirname(__DIR__, 2) . '/config/config.php';
        if (!\file_exists($configPath)) {
            throw new \RuntimeException('Error, config.php doesn\'t exist, please ensure you copy config.sample.php and edit accordingly');
        }

        $this->config = collect(require_once($configPath));
    }

    public function __get(string $key): mixed
    {
        return $this->config->get($key);
    }
}
