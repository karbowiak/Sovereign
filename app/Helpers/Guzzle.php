<?php

namespace Sovereign\Helpers;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;

class Guzzle
{
    public function __construct(
        protected Client $guzzle,
        protected Logger $logger
    ) {
    }

    public function request(string $url, array $options = [], string $requestType = 'GET', int $ttl = 300)
    {
        $md5 = md5($url);
        $cachedData = $this->getCached($md5);
        if (is_null($cachedData)) {
            $result = $this->fetch($url, $options, $requestType);
            $this->saveCached($md5, $result, $ttl);
            return $result;
        }
        return $cachedData;
    }

    private function fetch(string $url, array $options, string $requestType): mixed
    {
        try {
            $request = $this->guzzle->request($requestType, $url, $options);
            if ($request->getStatusCode() === 200) {
                $body = $request->getBody()->getContents();

                try {
                    return json_decode($body, true, 512, JSON_THROW_ON_ERROR);
                } catch (Exception $e) {
                    $this->logger->error('Error decoding request: ' . $e->getMessage());
                    return [];
                }
            }
        } catch (GuzzleException $e) {
            $this->logger->error('Error requesting data: ' . $e->getMessage());
            return [];
        }
    }

    private function saveCached(string $md5, mixed $data, int $ttl): void
    {
        \apcu_add($md5, $data, $ttl);
    }

    private function getCached(string $md5): mixed
    {
        $result = \apcu_fetch($md5);
        if (empty($result)) {
            return null;
        }

        return $result;
    }
}
