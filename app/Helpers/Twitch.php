<?php

namespace Sovereign\Helpers;

class Twitch
{
    public function __construct(
        protected Guzzle $guzzle,
        protected Logger $logger,
        protected Config $config
    ) {
    }

    public function fetchLiveInfo(string $streamer): array
    {
        $request = $this->guzzle->request('https://api.twitch.tv/helix/streams?user_login=' . $streamer, [
            'headers' => [
                'Client-ID' => $this->config->twitchClientId,
                'Authorization' => 'Bearer ' . $this->config->twitchAppOauth
            ]
        ]);
        return $request['data'][0] ?? [];
    }

    public function fetchStreamerInfo(string $streamer): array
    {
        $request = $this->guzzle->request('https://api.twitch.tv/helix/users?login=' . $streamer, [
            'headers' => [
                'Client-ID' => $this->config->twitchClientId,
                'Authorization' => 'Bearer ' . $this->config->twitchAppOauth
            ]
        ]);
        return $request['data'][0] ?? [];
    }

    public function getTime(string $startTime): string
    {
        $currentTime = time();
        $startTime = strtotime($startTime);
        $totalSeconds = $currentTime - $startTime;

        $days = (int) $totalSeconds / (3600 * 24);
        $hours = (int) ($totalSeconds / 3600) % 24;
        $minutes = (int) ($totalSeconds / 60) % 60;
        $seconds = (int) $totalSeconds % 60;

        $result = '';
        if ($days >= 1) {
            $result .= "{$days}d ";
        }
        if ($hours >= 1) {
            $result .= "{$hours}h ";
        }
        if ($minutes >= 1) {
            $result .= "{$minutes}m ";
        }
        $result .= "{$seconds}s";
        return $result;
    }
}
