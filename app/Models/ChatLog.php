<?php

namespace Sovereign\Models;

use Sovereign\Database\MongoCollection;

class ChatLog extends MongoCollection
{
    public string $collectionName = 'chatlog';
    public string $indexField = 'messageId';
}
