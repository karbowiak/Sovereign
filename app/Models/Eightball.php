<?php

namespace Sovereign\Models;

use Sovereign\Database\MongoCollection;

class Eightball extends MongoCollection
{
    public string $collectionName = 'eightball';
    public string $seedFile = 'eightball';
}
