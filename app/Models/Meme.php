<?php

namespace Sovereign\Models;

use Sovereign\Database\MongoCollection;

class Meme extends MongoCollection
{
    public string $collectionName = 'meme';
    public string $seedFile = 'meme';
}
