<?php

namespace Sovereign\Models;

use Sovereign\Database\MongoCollection;

class Nudes extends MongoCollection
{
    public string $collectionName = 'nudes';
    public string $indexField = 'key';
    public string $seedFile = 'nudes';
}
