<?php

namespace Sovereign\Models;

use Sovereign\Database\MongoCollection;

class OpenAILog extends MongoCollection
{
    public string $collectionName = 'openailog';
    public string $indexField = 'messageId';
}
