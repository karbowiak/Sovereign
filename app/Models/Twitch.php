<?php

namespace Sovereign\Models;

use Sovereign\Database\MongoCollection;

class Twitch extends MongoCollection
{
    public string $collectionName = 'twitch';
    public string $indexField = 'name';
    public string $seedFile = 'twitch';
}
