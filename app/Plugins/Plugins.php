<?php

namespace Sovereign\Plugins;

use Composer\Autoload\ClassLoader;
use Illuminate\Support\Collection;
use Kcs\ClassFinder\Finder\ComposerFinder;
use League\Container\Container;
use Sovereign\Api\onTriggerInterface;
use Sovereign\Api\onVoiceInterface;

class Plugins
{
    protected Collection $onMessage;
    protected Collection $onTimer;
    protected Collection $onVoice;
    protected Collection $onTrigger;

    public function __construct(
        protected ClassLoader $autoloader,
        protected Container $container
    ) {
        // Init the onMessage, onTimer and onVoice collections
        $this->onMessage = new Collection();
        $this->onTimer = new Collection();
        $this->onVoice = new Collection();
        $this->onTrigger = new Collection();

        $this->boot();
    }

    protected function boot(): void
    {
        $pluginFinder = new ComposerFinder();
        $pluginFinder->inNamespace('Sovereign\\Plugins');
        foreach ($pluginFinder as $className => $reflection) {
            if (\str_contains($className, 'Sovereign\\Plugins\\onMessage')) {
                $this->onMessage->add($this->container->get($className));
            } elseif (\str_contains($className, 'Sovereign\\Plugins\\onTimer')) {
                $this->onTimer->add($this->container->get($className));
            } elseif (\str_contains($className, 'Sovereign\\Plugins\\onVoice')) {
                /** @var onVoiceInterface $instance */
                $instance = $this->container->get($className);
                $triggers = $instance->getTriggers();
                foreach ($triggers as $trigger) {
                    $this->onVoice->put($trigger, $instance);
                }
            } elseif (\str_contains($className, 'Sovereign\\Plugins\\onTrigger')) {
                /** @var onTriggerInterface $instance */
                $instance = $this->container->get($className);
                $triggers = $instance->getTriggers();
                foreach ($triggers as $trigger) {
                    $this->onTrigger->put($trigger, $instance);
                }
            }
        }
    }

    public function getOnMessage(): Collection
    {
        return $this->onMessage;
    }

    public function getOnTimer(): Collection
    {
        return $this->onTimer;
    }

    public function getOnVoice(?string $trigger = null): null|Collection|onVoiceInterface
    {
        return !is_null($trigger) ? $this->onVoice->get($trigger) : $this->onVoice;
    }

    public function getOnTrigger(?string $trigger = null): null|Collection|onTriggerInterface
    {
        return !is_null($trigger) ? $this->onTrigger->get($trigger) : $this->onTrigger;
    }
}
