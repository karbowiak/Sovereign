<?php

namespace Sovereign\Plugins\onMessage;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Sentiment\Analyzer;
use Sovereign\Api\onMessageInterface;
use Sovereign\Helpers\Logger;
use Sovereign\Models\ChatLog;

class EchoLog extends onMessageInterface
{
    public function __construct(
        protected Logger $logger,
        protected ChatLog $chatLog
    ) {
    }

    public function handle(Discord $discord, Message $message): void
    {
        // Ignore messages from the bot itself
        if ($message->author->id !== $discord->id) {
            $name = !empty($message->author->nick) ? $message->author->nick : $message->author->username;
            $content = $this->changeIdsToNames($message->content, $message);

            $analyzer = new Analyzer();
            $sentiment = $analyzer->getSentiment($content);

            $this->logger->info("-{$sentiment['neg']} / ~{$sentiment['neu']} / +{$sentiment['pos']} | {$name} | #{$message->channel->name} | {$content}");
            $this->chatLog->collection->insertOne([
                'messageId' => $message->id,
                'channelId' => $message->channel_id,
                'channelName' => $message->channel->name,
                'authorName' => $name,
                'authorId' => $message->author->id,
                'content' => $content,
                'sentiment' => $sentiment
            ], ['upsert' => true]);
        }
    }

    private function changeIdsToNames(string $content, Message $message)
    {
        try {
            $regexes = ['/(?<=<@!).+?(?=>)/m', '/(?<=<@).+?(?=>)/m'];
            foreach ($regexes as $regex) {
                preg_match_all($regex, $content, $matches, \PREG_SET_ORDER, 0);
                $matches = array_merge($matches);
                foreach ($matches as $match) {
                    /** @var \Discord\Parts\User\Member $member */
                    $member = $message->channel->guild->members->get('id', $match[0]);
                    $name = $member->nick ?? $member->username;
                    $content = str_replace(['<@!' . $match[0] . '>', '<@' . $match[0] . '>'], $name, $content);
                }
            }
        } catch (\Exception $e) {
            dump('Exception caused while trying to change IDs to names in the content', $e->getMessage());
        }

        return $content;
    }
}
