<?php

namespace Sovereign\Plugins\onMessage;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Discord\Parts\Guild\Guild;
use Discord\Parts\User\User;
use GuzzleHttp\Client;
use Sovereign\Api\onMessageInterface;
use Sovereign\Helpers\Config;
use Sovereign\Helpers\Logger;
use Sovereign\Models\OpenAILog;

class OpenAI extends onMessageInterface
{
    public function __construct(
        protected Logger $logger,
        protected OpenAILog $openAILog,
        protected Config $config
    ) {
    }

    public function handle(Discord $discord, Message $message): void
    {
        // Ignore messages from the bot itself
        if (($message->author->id !== $discord->id && \str_contains($message->content, $discord->id)) || ($message->author->id !== $discord->id && $message->channel->guild_id === null)) {
            $question = trim(str_replace('<@!' . $discord->id . '>', '', $message->content));
            $question = $this->changeIdsToNames($question, $message);

            $prompt = "Sovereign is a chatbot that reluctantly answers questions.\n\nHuman: How many pounds are in a kilogram?\nAI: Again? There are 2.2 pounds in a kilogram. Please remember it this time.";
            $messageCount = $this->openAILog->count(['channelId' => $message->channel_id, 'prompt' => 'default']);
            $limit = $message->channel->guild_id === null ? 20 : 5;
            $lastFive = $this->openAILog->find([
                'channelId' => $message->channel_id,
                'prompt' => 'default'
            ], [
                'limit' => $limit,
                'sort' => ['createdAt' => 1],
                'skip' => $messageCount >= $limit ? $messageCount - $limit : 0
            ]);

            foreach ($lastFive as $qa) {
                $qq = $qa['question'];
                $answer = $qa['response'];
                $prompt .= "\nHuman: {$qq}.\nAI:{$answer}";
            }
            $prompt .= "\nHuman: {$question}.\nAI:";

            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->config->openAiToken
                ]
            ]);

            $response = $client->post('https://api.openai.com/v1/engines/davinci/completions', [
                'json' => [
                    'prompt' => $prompt,
                    'max_tokens' => 1500,
                    'temperature' => 0.9,
                    'top_p' => 1,
                    'presence_penalty' => 0.3,
                    'frequency_penalty' => 0.3,
                    'n' => 1,
                    'stream' => false,
                    'logprobs' => null,
                    'stop' => ["\n", " Human:", " AI:"],
                    'best_of' => 1
                ]
            ]);

            if ($response->getStatusCode() === 200) {
                $content = $response->getBody()->getContents();
                $content = json_decode($content, true);
                $text = trim($content['choices'][0]['text']);

                $this->openAILog->collection->insertOne([
                    'messageId' => $message->id,
                    'question' => $question,
                    'response' => $text,
                    'authorId' => $message->author->id,
                    'channelId' => $message->channel_id,
                    'createdAt' => $this->openAILog->makeTimeFromUnixTime(time()),
                    'prompt' => 'default'
                ]);
                $this->logger->info('OpenAI Question: ' . $question . ' | OpenAI Response: ' . $text, $content);
                $message->channel->sendMessage($text);
            }
        }
    }

    private function changeIdsToNames(string $question, Message $message)
    {
        $regex = '/(?<=<@!).+?(?=>)/m';
        preg_match_all($regex, $question, $matches, \PREG_SET_ORDER, 0);
        $matches = array_merge($matches);
        foreach ($matches as $match) {
            /** @var \Discord\Parts\User\Member $member */
            $member = $message->channel->guild->members->get('id', $match[0]);
            $name = $member->nick ?? $member->username;
            $question = str_replace('<@!' . $match[0] . '>', $name, $question);
        }

        return $question;
    }
}
