<?php

namespace Sovereign\Plugins\onTimer;

use Discord\Discord;
use Sovereign\Api\onTimerInterface;
use Sovereign\Helpers\Config;

class AutoFriday extends onTimerInterface
{
    protected int $interval = 60; // every 1m

    public function __construct(
        protected Config $config
    ) {
    }

    public function handle(Discord $discord): void
    {
        $dayOfWeek = date('w');
        $hour = date('H');
        $minute = date('i');
        if ($dayOfWeek === '5' && $hour === '13' && $minute === '00') {
            $links = ['https://www.youtube.com/watch?v=iCFOcqsnc9Y', 'https://www.youtube.com/watch?v=kfVsfOSbJY0'];
            $channelId = $this->config->fridayChannel;
            $channel = $discord->getChannel($channelId);
            $channel->sendMessage("@here IT'S FRIDAY MY DUDES.. {$links[array_rand($links)]}");
        }
    }
}
