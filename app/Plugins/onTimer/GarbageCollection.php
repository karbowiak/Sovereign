<?php

namespace Sovereign\Plugins\onTimer;

use Discord\Discord;
use Sovereign\Api\onTimerInterface;
use Sovereign\Helpers\Logger;

class GarbageCollection extends onTimerInterface
{
    protected int $interval = 900; // every 15m

    public function __construct(
        protected Logger $logger
    ) {
    }

    public function handle(Discord $discord): void
    {
        $this->logger->info('Collecting garbage..');
        gc_collect_cycles();
        $this->logger->info(
            'Memory usage is now: ' .
            round(memory_get_usage() / 1024 / 1024) .
            'MB (Peak: ' .
            round(memory_get_peak_usage() / 1024 / 1024) .
            'MB)'
        );
    }
}
