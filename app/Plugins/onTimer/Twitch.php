<?php

namespace Sovereign\Plugins\onTimer;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Discord\Parts\Embed\Embed;
use Sovereign\Api\onTimerInterface;
use Sovereign\Helpers\Config;
use Sovereign\Helpers\Logger;

class Twitch extends onTimerInterface
{
    protected int $interval = 60;

    public function __construct(
        protected Logger $logger,
        protected Config $config,
        protected \Sovereign\Helpers\Twitch $twitch,
        protected \Sovereign\Models\Twitch $twitchModel
    ) {
    }

    public function handle(Discord $discord): void
    {
        $channel = $discord->getChannel($this->config->twitchNotificationChannel);
        $streamers = $this->twitchModel->find();

        /** @var \Illuminate\Support\Collection $streamer */
        foreach ($streamers as $streamer) {
            $streamerName = $streamer['name'];
            $announced = $this->hasAnnounced($streamerName);
            $offline = $this->isOffline($streamerName);

            if (!$announced && !$offline) {
                $streamerData = $this->twitch->fetchLiveInfo($streamerName);
                $streamerInfo = $this->twitch->fetchStreamerInfo($streamerName);
                if (empty($streamerData) || empty($streamerInfo)) {
                    continue;
                }

                $embed = new Embed($discord);
                $embed->setTitle($streamerData['user_name'] . ' is now live | ' . $streamerData['title']);
                $embed->setDescription($streamerData['game_name']);
                $embed->setThumbnail($streamerInfo['profile_image_url']);
                $embed->addFieldValues('Viewers', $streamerData['viewer_count']);
                $embed->addFieldValues('Uptime', $this->twitch->getTime($streamerData['started_at']));
                $embed->setImage(str_replace(['{width}', '{height}'], ['384', '216'], $streamerData['thumbnail_url']));
                $embed->setURL('https://twitch.tv/' . $streamerData['user_login']);

                try {
                    $channel->sendEmbed($embed)->then(function (Message $message) use ($streamerName) {
                        $this->setAnnounced($streamerName, $message->id);
                    });
                } catch (\Exception $e) {
                    $this->logger->error('Error sending Twitch embed: ' . $e->getMessage());
                }
            } elseif ($offline && $this->hasAnnounced($streamerName)) {
                $messageId = $this->unsetAnnounced($streamerName);
                $channel->getMessage($messageId)->then(function (Message $message) {
                    $message->delete();
                });
            }
        }
    }

    private function setAnnounced(string $streamer, string $messageId): void
    {
        dump("Message id set as:", $messageId);
        $this->twitchModel->collection->updateOne(['name' => $streamer], ['$set' => ['announced' => true, 'announceTime' => $this->twitchModel->makeTimeFromUnixTime(time()), 'messageId' => $messageId]]);
    }

    private function unsetAnnounced(string $streamer): ?string
    {
        $messageId = $this->twitchModel->findOne(['name' => $streamer])->get('messageId');
        dump("Message id found as:", $messageId);
        $this->twitchModel->collection->updateOne(['name' => $streamer], ['$set' => ['announced' => false, 'announceTime' => null, 'messageId' => null]]);

        return $messageId;
    }

    private function hasAnnounced(string $streamer): bool
    {
        return $this->twitchModel->findOne(['name' => $streamer])->get('announced', false);
    }

    private function isOffline(string $streamer): bool
    {
        $data = $this->twitch->fetchLiveInfo($streamer);
        if (!empty($data)) {
            return false;
        }

        return true;
    }
}
