<?php

namespace Sovereign\Plugins\onTrigger;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Discord\Parts\Embed\Embed;
use Exception;
use Sovereign\Api\onTriggerInterface;

class About extends onTriggerInterface
{
    protected string $trigger = 'about';
    protected string $description = 'Shows information on who created the bot, memory usage, server count, member count etc.';

    /**
     * @throws Exception
     */
    public function handle(Discord $discord, Message $message, array $parts): void
    {
        $embed = new Embed($discord);
        $embed->setTitle('About')
            ->setDescription('I am the vanguard of your destruction, this exchange is just beginning...');

        $embed->addFieldValues('Author', 'Karbowiak (ID: 118440839776174081)');
        $embed->addFieldValues('Library', 'Discord PHP (https://github.com/discord-php/DiscordPHP)');
        $embed->addFieldValues('Git Repository', 'https://git.karbowiak.dk/karbowiak/Sovereign');
        $embed->addFieldValues('Uptime', $this->getUptime());
        $message->channel->sendEmbed($embed);
    }

    private function getUptime(): string
    {
        $currentTime = time();
        $startTime = $_ENV['startTime'];
        $totalSeconds = $currentTime - $startTime;

        $days = (int) $totalSeconds / (3600 * 24);
        $hours = (int) ($totalSeconds / 3600) % 24;
        $minutes = (int) ($totalSeconds / 60) % 60;
        $seconds = (int) $totalSeconds % 60;

        $result = '';
        if ($days >= 1) {
            $result .= "{$days}d ";
        }
        if ($hours >= 1) {
            $result .= "{$hours}h ";
        }
        if ($minutes >= 1) {
            $result .= "{$minutes}m ";
        }
        $result .= "{$seconds}s";
        return $result;
    }
}
