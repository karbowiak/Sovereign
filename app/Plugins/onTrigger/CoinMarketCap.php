<?php

namespace Sovereign\Plugins\onTrigger;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Discord\Parts\Embed\Embed;
use Exception;
use Sovereign\Api\onTriggerInterface;
use Sovereign\Helpers\Config;
use Sovereign\Helpers\Guzzle;

class CoinMarketCap extends onTriggerInterface
{
    protected string $trigger = 'crypto';
    protected string $description = 'Shows the latest price from CoinMarketCap for a myriad of Crypto';

    public function __construct(
        protected Guzzle $guzzle,
        protected Config $config
    ) {
    }

    /**
     * @throws Exception
     */
    public function handle(Discord $discord, Message $message, array $parts): void
    {
        $symbol = $parts[1] ?? null;
        $data = $this->guzzle->request('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest', [
            'headers' => [
                'X-CMC_PRO_API_KEY' => $this->config->coinMarketCapApiToken
            ]
        ]);

        if (!is_null($symbol)) {
            $coin = array_filter(array_map(function ($coin) use ($symbol) {
                if ($coin['symbol'] === strtoupper($symbol)) {
                    return $coin;
                }
                return null;
            }, $data['data']));
            if (empty($coin)) {
                $message->channel->sendMessage('Error, no coin with that symbol was found..');
                return;
            }

            // Select the first element in the array, because we should only have one..
            $coin = $coin[array_key_first($coin)];
            $embed = new Embed($discord);
            $embed->setThumbnail('https://coinmarketcap.com/public/media/img/logo-square.png');
            $embed->setTitle('CoinMarketCap');
            $embed->setURL('https://coinmarketcap.com/');
            $embed->setDescription('Crypto value, from CoinMarketCap');
            $name = $coin['name'];
            $symbol = $coin['symbol'];
            $marketCap = number_format($coin['quote']['USD']['market_cap'], 2);
            $volume = $coin['quote']['USD']['volume_24h'];
            $usd = $coin['quote']['USD']['price'] < 1 ?
                number_format($coin['quote']['USD']['price'], 10, '.', '') :
                number_format($coin['quote']['USD']['price'], 4, '.', '');
            $oneHourChange = number_format($coin['quote']['USD']['percent_change_1h'], 2);
            $twentyFourHourChange = number_format($coin['quote']['USD']['percent_change_24h'], 2);
            $embed->addFieldValues("{$name} ({$symbol})", "{$usd} USD", true);
            $embed->addFieldValues('1h change', "{$oneHourChange} %", true);
            $embed->addFieldValues('24h change', "{$twentyFourHourChange} %", true);
            $embed->addFieldValues('24h trade volume', $volume);
            $embed->addFieldValues('Market Cap', $marketCap . ' USD');
        } else {
            $topEight = array_slice($data['data'], 0, 8);
            $embed = new Embed($discord);
            $embed->setThumbnail('https://coinmarketcap.com/public/media/img/logo-square.png');
            $embed->setTitle('CoinMarketCap');
            $embed->setURL('https://coinmarketcap.com/');
            $embed->setDescription('Crypto values, ranked by marketcap');
            $rank = 1;
            foreach ($topEight as $coin) {
                $name = $coin['name'];
                $symbol = $coin['symbol'];
                $usd = $coin['quote']['USD']['price'] < 1 ?
                    number_format($coin['quote']['USD']['price'], 10, '.', '') :
                    number_format($coin['quote']['USD']['price'], 4, '.', '');
                $oneHourChange = number_format($coin['quote']['USD']['percent_change_1h'], 2);
                $twentyFourHourChange = number_format($coin['quote']['USD']['percent_change_24h'], 2);

                $embed->addFieldValues("{$rank}. {$name} ({$symbol})", "{$usd} USD", true);
                $embed->addFieldValues('1h change', "{$oneHourChange} %", true);
                $embed->addFieldValues('24h change', "{$twentyFourHourChange} %", true);
                $rank++;
            }
        }

        $message->channel->sendEmbed($embed);
    }
}
