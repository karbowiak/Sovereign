<?php

namespace Sovereign\Plugins\onTrigger;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Sovereign\Api\onTriggerInterface;
use Sovereign\Models\Eightball as ModelsEightball;

class Eightball extends onTriggerInterface
{
    protected string $trigger = 'eightball';
    protected array $extraTriggers = ['eb'];
    protected string $description = 'Roll the eightball';

    public function __construct(
        protected ModelsEightball $eightball
    ) {
    }

    public function handle(Discord $discord, Message $message, array $parts): void
    {
        $answer = $this->eightball->find()->random(1)->first();
        $message->channel->sendMessage($answer['result']);
    }
}
