<?php

namespace Sovereign\Plugins\onTrigger;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Exception;
use Sovereign\Api\onTriggerInterface;

class Friday extends onTriggerInterface
{
    protected string $trigger = 'friday';
    protected string $description = 'It\'s friday!';

    /**
     * @throws Exception
     */
    public function handle(Discord $discord, Message $message, array $parts): void
    {
        $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        $dayOfWeek = date('w');
        $dayOfWeekName = $days[$dayOfWeek];
        if ($dayOfWeek === '5') {
            $links = ['https://www.youtube.com/watch?v=iCFOcqsnc9Y', 'https://www.youtube.com/watch?v=kfVsfOSbJY0'];
            $message->channel->sendMessage($links[array_rand($links)]);
        } else {
            $message->channel->sendMessage("Sorry my dude, it's not friday, it's actually {$dayOfWeekName}");
        }
    }
}
