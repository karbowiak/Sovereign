<?php

namespace Sovereign\Plugins\onTrigger;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Discord\Parts\Embed\Embed;
use Exception;
use Sovereign\Api\onTriggerInterface;

class Help extends onTriggerInterface
{
    protected string $trigger = 'help';
    protected string $description = 'Shows a list of commands, and what they do..';

    /**
     * @throws Exception
     */
    public function handle(Discord $discord, Message $message, array $parts): void
    {
        $voiceEmbed = new Embed($discord);
        $voiceEmbed->setTitle('Voice Commands');
        $voiceEmbed->setDescription('Use !<command> to execute');
        $added = [];
        foreach ($this->plugins->getOnVoice() as $plugin) {
            if (!\in_array($plugin->getTrigger(), $added, true)) {
                $voiceEmbed->addFieldValues(implode(', ', $plugin->getTriggers()), $plugin->getDescription());
                $added[] = $plugin->getTrigger();
            }
        }

        $triggerEmbed = new Embed($discord);
        $triggerEmbed->setTitle('Trigger Commands');
        $voiceEmbed->setDescription('Use !<command> to execute');
        foreach ($this->plugins->getOnTrigger() as $plugin) {
            if (!\in_array($plugin->getTrigger(), $added, true)) {
                $triggerEmbed->addFieldValues(implode(', ', $plugin->getTriggers()), $plugin->getDescription());
                $added[] = $plugin->getTrigger();
            }
        }

        $message->channel->sendEmbed($voiceEmbed)->then(function (Message $message) use ($triggerEmbed) {
            $message->channel->sendEmbed($triggerEmbed);
        });
    }
}
