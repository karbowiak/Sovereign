<?php

namespace Sovereign\Plugins\onTrigger;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Exception;
use Sovereign\Api\onTriggerInterface;
use Sovereign\Models\Meme as ModelsMeme;

class Meme extends onTriggerInterface
{
    protected string $trigger = 'meme';
    protected string $description = 'Shows a random meme';

    public function __construct(
        protected ModelsMeme $meme
    ) {
    }
    /**
     * @throws Exception
     */
    public function handle(Discord $discord, Message $message, array $parts): void
    {
        $result = $this->meme->find()->random(1)->first();
        $message->channel->sendMessage($result['result']);
    }
}
