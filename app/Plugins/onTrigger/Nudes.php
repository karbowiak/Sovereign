<?php

namespace Sovereign\Plugins\onTrigger;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Exception;
use Sovereign\Api\onTriggerInterface;
use Sovereign\Helpers\Guzzle;
use Sovereign\Models\Nudes as ModelsNudes;

class Nudes extends onTriggerInterface
{
    protected string $trigger = 'nudes';
    protected string $description = 'Shows a random meme';

    public function __construct(
        protected Guzzle $guzzle,
        protected ModelsNudes $nudes
    ) {
    }
    /**
     * @throws Exception
     */
    public function handle(Discord $discord, Message $message, array $parts): void
    {
        // @TODO make this actually use the list, instead of as it is now where the list is only used as a pointer as to what you could type..
        $subreddits = $this->nudes->find();
        $selected = $parts[1] ?? null;
        if (empty($selected)) {
            $message->channel->sendMessage('Error, no subreddit selected, please type one of: ' . implode(', ', $subreddits->all()));
            return;
        }

        $request = $this->guzzle->request('https://reddit.com/r/' . $selected . '/new.json');
        $children = array_values(array_map(static function ($child) {
            if ($child['kind'] === 't3') {
                return $child;
            }
            return null;
        }, $request['data']['children'] ?? []));

        if (!empty($children)) {
            $image = $children[array_rand($children)]['data'];
            $url = $image['url'];

            try {
                $message->channel->sendMessage($url);
            } catch (\Exception $e) {
                $message->channel->sendMessage('Sorry, error occurred: ' . $e->getMessage());
            }
        } else {
            $message->channel->sendMessage('Sorry, no images available');
        }
    }
}
