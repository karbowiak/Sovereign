<?php

namespace Sovereign\Plugins\onTrigger;

use Discord\Discord;
use Discord\Parts\Channel\Message;
use Discord\Parts\Embed\Embed;
use Exception;
use GuzzleHttp\Client;
use Sovereign\Api\onTriggerInterface;
use Sovereign\Models\Twitch as ModelsTwitch;

class Twitch extends onTriggerInterface
{
    protected string $trigger = 'twitch';
    protected string $description = 'Shows information on a Twitch streamer';

    public function __construct(
        protected Client $guzzle,
        protected \Sovereign\Helpers\Twitch $twitch,
        protected ModelsTwitch $twitchModel
    ) {
    }

    /**
     * @throws Exception
     */
    public function handle(Discord $discord, Message $message, array $parts): void
    {
        $command = $parts[1] ?? null;
        $streamer = $parts[2] ?? null;
        $validCommands = ['add', 'list', 'remove', 'info'];

        if (is_null($command)) {
            $message->channel->sendMessage('Error, you have to define a command, valid commands are: ' . implode(', ', $validCommands));
            return;
        }
        if ($command !== 'list' && is_null($streamer)) {
            $message->channel->sendMessage('Error, you have to write a name of the streamer you want information on.');
            return;
        }

        switch ($command) {
            case 'addMany':
                $manyStreamers = explode(',', $streamer);
                $added = false;
                foreach ($manyStreamers as $streamer) {
                    $this->twitchModel->collection->insertOne(['name' => $streamer, 'announced' => false, 'announceTime' => null, 'messageId' => null]);
                }

                if ($added === true) {
                    $message->channel->sendMessage('Streamers added: \`\`\`' . implode(', ', $manyStreamers) . '\`\`\`');
                }
                break;
            case 'add':
                $result = $this->twitchModel->collection->insertOne(['name' => $streamer, 'announced' => false, 'announceTime' => null, 'messageId' => null]);

                if ($result->isAcknowledged() && $result->getInsertedCount() > 0) {
                    $message->channel->sendMessage('Streamer has been added');
                }
                break;

            case 'list':
                $streamers = $this->twitchModel->find();
                if (!$streamers->isEmpty()) {
                    $message->channel->sendMessage('```' . $streamers->implode('name', ', ') . '```');
                } else {
                    $message->channel->sendMessage('No streamers on the list');
                }
                break;

            case 'remove':
                $result = $this->twitchModel->delete(['name' => $streamer]);
                if ($result->isAcknowledged() && $result->getDeletedCount() > 0) {
                    $message->channel->sendMessage('Streamer has been removed');
                }
                break;

            case 'info':
                $info = $this->twitch->fetchStreamerInfo($streamer);
                $liveStatus = $this->twitch->fetchLiveInfo($streamer);
                if (!empty($info)) {
                    $embed = new Embed($discord);
                    $embed->setTitle($info['display_name']);
                    $embed->setDescription($info['description']);
                    if (!empty($liveStatus)) {
                        $embed->setDescription('LIVE | ' . $liveStatus['title']);
                        $embed->setThumbnail($info['profile_image_url']);
                        $embed->setImage(str_replace(['{width}', '{height}'], ['384', '216'], $liveStatus['thumbnail_url']));
                        $embed->addFieldValues('Viewers', $liveStatus['viewer_count']);
                    } else {
                        $embed->setImage($info['profile_image_url']);
                    }
                    $embed->setURL('https://twitch.tv/' . $info['login']);
                    $embed->addFieldValues('Views', $info['view_count'])->addFieldValues('Created', date('Y-m-d H:i:s', strtotime($info['created_at'])));

                    $message->channel->sendEmbed($embed);
                }
                break;
        }
    }
}
