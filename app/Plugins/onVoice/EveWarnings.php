<?php

namespace Sovereign\Plugins\onVoice;

use Discord\Discord;
use Discord\Voice\VoiceClient;
use Exception;
use React\Promise\ExtendedPromiseInterface;
use Sovereign\Api\onVoiceInterface;

class EveWarnings extends onVoiceInterface
{
    protected string $trigger = 'eve';
    protected string $description = 'Plays a random EVE warning sound';

    /**
     * @throws Exception
     */
    public function handle(VoiceClient $vc, Discord $discord): ExtendedPromiseInterface
    {
        $file = dirname(__DIR__, 3) . '/resources/audio/eve/' . random_int(1, 6) . '.mp3';
        return $vc->playFile($file);
    }
}
