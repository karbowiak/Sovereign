<?php

namespace Sovereign\Plugins\onVoice;

use Discord\Discord;
use Discord\Voice\VoiceClient;
use Exception;
use React\Promise\ExtendedPromiseInterface;
use Sovereign\Api\onVoiceInterface;

class Horn extends onVoiceInterface
{
    protected string $trigger = 'horn';
    protected string $description = 'It\'s horny time!';

    /**
     * @throws Exception
     */
    public function handle(VoiceClient $vc, Discord $discord): ExtendedPromiseInterface
    {
        $file = dirname(__DIR__, 3) . '/resources/audio/horns/' . random_int(1, 6) . '.mp3';
        return $vc->playFile($file);
    }
}
