<?php

namespace Sovereign\Plugins\onVoice;

use Discord\Discord;
use Discord\Voice\VoiceClient;
use Exception;
use React\Promise\ExtendedPromiseInterface;
use Sovereign\Api\onVoiceInterface;

class Sovereign extends onVoiceInterface
{
    protected string $trigger = 'sovereign';
    protected string $description = 'Plays a random quote from Sovereign';

    /**
     * @throws Exception
     */
    public function handle(VoiceClient $vc, Discord $discord): ExtendedPromiseInterface
    {
        $file = dirname(__DIR__, 3) . '/resources/audio/reapers/' . random_int(1, 23) . '.mp3';
        return $vc->playFile($file);
    }
}
