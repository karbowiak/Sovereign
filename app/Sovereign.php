<?php

namespace Sovereign;

use Discord\Discord;
use Discord\Exceptions\IntentException;
use Discord\WebSockets\Intents;
use JetBrains\PhpStorm\NoReturn;
use JetBrains\PhpStorm\Pure;
use League\Container\Container;
use React\EventLoop\LoopInterface;
use React\EventLoop\StreamSelectLoop;
use Sovereign\Helpers\Config;
use Sovereign\Helpers\Logger;

class Sovereign
{
    protected Discord $bot;
    protected StreamSelectLoop $loop;

    public function __construct(
        protected Container $container,
        protected Logger $logger
    ) {
    }

    /**
     * @throws IntentException
     */
    public function getBotInstance(): Discord
    {
        $token = $this->container->get(Config::class)->token;

        $this->bot = new Discord([
            'token' => $token,
            'loadAllMembers' => true,
            'intents' => Intents::getDefaultIntents() | Intents::GUILD_MEMBERS | Intents::GUILD_VOICE_STATES,
            'logger' => $this->logger
        ]);

        // Add the bot to the container
        $this->container->add(__CLASS__, $this);

        return $this->bot;
    }

    #[NoReturn]
    public function exit(): void
    {
        die();
    }

    #[Pure]
    public function getLoop(): LoopInterface
    {
        return $this->bot->getLoop();
    }
}
