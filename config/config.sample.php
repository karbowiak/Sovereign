<?php

return [
    'token' => '',
    'redditClientId' => '',
    'redditClientSecret' => '',
    'twitchClientId' => '',
    'twitchAppOauth' => '',
    'twitchNotificationChannel' => '',
    'coinmarketcapApipToken' => '',
    'openAiToken' => '',
    'mongoOptions' => isset($_SERVER['MONGO_OPTIONS']) ? $_SERVER['MONGO_OPTIONS'] : 'connectTimeoutMS=50000,socketTimeoutMS=50000',
    'mongoHost' => isset($_SERVER['MONGO_HOST']) ? $_SERVER['MONGO_HOST'] : '127.0.0.1:27017'
];
